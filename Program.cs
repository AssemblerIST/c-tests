﻿using System;


namespace Hello_world
{  
    interface IBoss
    {
        string Name { get; set; }
        void Print();
    }

    interface IArmor
    {
        void PrintArmor();
    }

    class Maskan : IBoss, IArmor
    {
        public string Name { get; set; }
        public void Print()
        {
            Console.WriteLine("I'm " + Name +  " Boss");
        }
        public void PrintArmor()
        {
            Console.WriteLine("Armor for boss " + Name);
        }
    }

    // интерфейсы нужны для множественного наследования

    class Program
    {
        static void Main(string[] args)
        {
            Maskan boss = new Maskan();
            boss.Name = Console.ReadLine();
            boss.Print();
            boss.PrintArmor();
        }
    }
}
